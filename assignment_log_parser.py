import os
import os.path

import csv
import sys

BASE = './files'


def get_required_data(line):
    if line.find('ELB-HealthChecker') <= -1:
        try:
            date_time = ' '.join(line.strip().split(' ')[4:6]).replace('[', '').replace(']', '')
            user_id = int(line.strip().split(' ')[7].split('/')[-1])
            user_agent = line.strip().split('"')[-2]

            return date_time, user_id, user_agent
        except Exception:
            # print 'userid is of not the right format or it is wrong'
            return None
    else:
        # print 'Its a simple health check'
        pass


def write_required_data(data, path):
    with open(os.path.join(BASE, 'user_info.csv'), 'a') as o:
        csv_writer = csv.writer(o, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        for item in data:
            csv_writer.writerow(item)


def create_folders(structure):
    years = structure.keys()

    for year in years:

        if not os.path.exists(os.path.join('.', year)):
            os.makedirs(os.path.join('.', year))

        months = structure.get(year).keys()

        for month in months:
            if not os.path.exists(os.path.join('.', year, month)):
                os.makedirs(os.path.join('.', year, month))

            days = structure.get(year).get(month).keys()

            for day in days:
                if not os.path.exists(os.path.join('.', year, month, day)):
                    os.makedirs(os.path.join('.', year, month, day))

                items = structure.get(year).get(month).get(day)

                with open(os.path.join('.', year, month, day, 'user_info.csv'), 'a') as o:
                    csv_writer = csv.writer(o, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
                    for item in items:
                        csv_writer.writerow(item)


def main():
    required_data = []
    files = os.listdir(BASE)

    for file_name in files:
        with open(os.path.join(BASE, file_name), 'r') as f:
            for line in f.readlines():
                data = get_required_data(line.strip())
                if data is not None:
                    required_data.append(data)

    structure = {}

    for item in required_data:
        date, month, year = item[0].split(":")[0].split("/")

        if structure.get(year) is not None:
            if structure.get(year).get(month) is not None:
                if structure.get(year).get(month).get(date) is not None:
                    structure.get(year).get(month).get(date).append(item)
                else:
                    structure.get(year).get(month)[date] = []
            else:
                structure.get(year)[month] = {}
        else:
            structure[year] = {}

    create_folders(structure)


if __name__ == '__main__':
    main()
