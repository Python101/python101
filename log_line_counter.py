from datetime import datetime
import sys


def get_date_time(line):
    if line.find('ELB-HealthChecker') <= -1:
        try:
            date_time = ' '.join(line.strip().split(' ')[4:5]).replace('[', '').replace(']', '')
            return date_time
        except Exception:
            # print 'userid is of not the right format or it is wrong'
            return None
    else:
        # print 'Its a simple health check'
        pass


with open('./files/access_log-1475218861') as log_file:

    result = {}

    for line in log_file.readlines():
        date_time = get_date_time(line)
        if date_time is not None:
            try:
                d = datetime.strptime(date_time, '%d/%b/%Y:%H:%M:%S')
                key = d.strftime('%Y-%m-%d %H:%M:%S')

                if result.get(key) is None:
                    result[key] = 1
                else:
                    result[key] = result.get(key) + 1
            except Exception:
                print sys.exc_info()

    for key, value in result.iteritems():
        print key, value

