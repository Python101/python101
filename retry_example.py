import random
from datetime import datetime
from retrying import retry


# wait_fixed
# wait_random_min
# wait_random_max
# wait_exponential_multiplier
# wait_exponential_max
# stop_max_delay
# stop_max_attempt_number
# retry_on_result
# retry_on_exception


def display_time():
    print datetime.now().strftime('%Y-%m-%d %H:%M:%S')


@retry
def pick_one():
    random_number = random.randint(0, 10)
    if random_number != 1:
        print 'picked', random_number
        raise Exception('One not picked!')
    else:
        print 'Picked 1 Finally'


@retry(wait_fixed=2000, stop_max_attempt_number=4)
def wait_fixed():
    display_time()
    raise Exception('Retry!')


@retry(wait_exponential_multiplier=1000, wait_exponential_max=10000, stop_max_delay=30000)
def retry_exponential():
    display_time()
    raise Exception('Retry!')


def retry_on_ioerror(exc):
    return isinstance(exc, IOError)


@retry(retry_on_exception=retry_on_ioerror)
def retry_on_exception():
    raise IOError


def retry_on_none(result):
    return result is None


@retry(retry_on_result=retry_on_none)
def retry_on_result():
    print 'Retry for ever if return value is None'


if __name__ == '__main__':
    wait_fixed()
