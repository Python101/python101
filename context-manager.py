import contextlib
import sys


class LoggingContextManager:

    def __init__(self):
        pass

    def __enter__(self):
        print 'LoggingContextManager.__enter__()'
        return 'Hello, World!'

    def __exit__(self, exc_type, exc_val, exc_tb):
        print 'LoggingContextManager.__exit__({}, {}, {})'.format(exc_type, exc_val, exc_tb)
        return


@contextlib.contextmanager
def logging_context_manager():
    print 'logging_context_manager.__enter__()'
    try:
        yield 'Hello, World!'
        print 'logging_context_manager.__exit__()'
    except Exception:
        print 'Exception exit', sys.exc_info()
        raise


@contextlib.contextmanager
def nest_test(name):
    print 'Entering ', name
    yield name
    print 'Exiting ', name


def main():
    with nest_test('outer') as n1, nest_test('inner nested in ' + n1):
        print 'BODY'


def main2():

    with LoggingContextManager() as l:
        print 'Business Logic'


if __name__ == '__main__':
    main2()
