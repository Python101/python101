from datetime import datetime, timedelta

start = datetime.strptime('2016-10-05 00:00:00', '%Y-%m-%d %H:%M:%S')
stop = datetime.strptime('2016-10-06 00:00:00', '%Y-%m-%d %H:%M:%S')


while stop > start:
    print start.strftime('%Y-%m-%d %H:%M:%S')
    start += timedelta(minutes=10)
